<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Console;

/**
 * Console message helper.
 */
class Prints
{
    /**
     * Maximum amount of characters to display on one console print line.
     *
     * @var int
     */
    public const MAX_CHARS = 80;

    /**
     * Prefix for log print commands.
     *
     * @var string
     */
    public const LOG_PREFIX = '';

    /**
     * Default verbose value for printing event messages.
     *
     * @var int
     */
    public const VERBOSE_DEFAULT = 0;

    /**
     * Verbose value to display all event messages.
     *
     * @var int
     */
    public const VERBOSE_DEBUG = 2;

    /**
     * Verbose value to display additional event messages.
     *
     * @var int
     */
    public const VERBOSE_VERBOSE = 1;

    /**
     * Display event messages level.
     *
     * @var int
     */
    public static $verbose = self::VERBOSE_DEFAULT;

    /**
     * Print a hard wrapped block of text.
     *
     * @param string $text    Text to print
     * @param string $padding Starting indentation
     * @param string $indent  Horizontal padding for vertical alignment
     */
    public static function hardWrap(string $text, string $padding = '', string $indent = '')
    {
        $words = array_filter(explode(' ', $text), 'trim');
        $stream = $padding;

        foreach ($words as $word) {
            $buffer = $stream.('' === $stream ? '' : ' ').$word;

            if (strlen($buffer) > self::MAX_CHARS) {
                // Print current line
                self::p($stream);

                // Every new line is indented
                $stream = $indent.$word;
            } else {
                // Update buffer and continue looping
                $stream = $buffer;
            }
        }

        // Prints last line
        self::p($stream);
    }

    /**
     * Prints a given message as a prefixed log.
     *
     * @param string $message String to print
     */
    public static function l(string $message)
    {
        self::p(self::LOG_PREFIX.$message);
    }

    /**
     * Prints a given message.
     *
     * @param string $message String to print
     * @param string $eol     End of line terminator
     */
    public static function p(string $message = '', string $eol = "\n")
    {
        echo $message.$eol;
    }

    /**
     * Prints a given message if verbose mode is enabled.
     *
     * @param string $message String to print
     * @param string $id      Optional identifier for identifying method caller
     */
    public static function v(string $message, string $id = '')
    {
        if (self::$verbose >= self::VERBOSE_VERBOSE) {
            self::l('(verbose'.($id ? ':'.$id : '').') '.$message);
        }
    }

    /**
     * Prints a given message if debug mode is enabled.
     *
     * @param string $message String to print
     * @param string $id      Optional identifier for identifying method caller
     */
    public static function vv(string $message, string $id = '')
    {
        if (self::$verbose >= self::VERBOSE_DEBUG) {
            self::l('(debug'.($id ? ':'.$id : '').') '.$message);
        }
    }
}
