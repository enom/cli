<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Console;

/**
 * Command line parser.
 */
class CommandLine
{
    /**
     * Parsed command line arguments.
     *
     * @var array
     */
    public static $args = [];

    /**
     * Reads $_SERVER['argv'] and provides easy access to parameters.
     *
     * @param bool $boolish Convert (e.g.) "yes" to `true`
     */
    public function __construct(bool $boolish = true)
    {
        static::$args = static::parse($_SERVER['argv']);

        if ($boolish) {
            foreach (static::$args as $arg => $value) {
                if (null !== ($bool = static::boolish($value))) {
                    static::$args[$arg] = $bool;
                }
            }
        }
    }

    /**
     * Attempts to interpret a given value as a boolean.
     *
     * Useful for command line handling of "boolean" arguments.
     *
     * @param mixed $value Value to interpret
     *
     * @return bool|null
     */
    public static function boolish($value)
    {
        if (is_string($value)) {
            $value = strtolower($value);
        }

        if (static::trueish($value)) {
            return true;
        }

        if (static::falseish($value)) {
            return false;
        }
    }

    /**
     * Checks to see if the value could be interpreted as 'true'.
     *
     * @param mixed $value Value to check
     *
     * @return bool
     */
    public static function trueish($value)
    {
        return in_array($value, [true, 1, 'y', 'yes', 'true', '1', 'on'], true);
    }

    /**
     * Checks to see if the value could be interpreted as 'false'.
     *
     * @param mixed $value Value to check
     *
     * @return bool
     */
    public static function falseish($value)
    {
        return in_array($value, [false, 0, 'n', 'no', 'false', '0', 'off'], true);
    }

    /**
     * Returns the interpreted $_SERVER['argv'].
     *
     * @return array
     */
    public function args()
    {
        return static::$args;
    }

    /**
     * Fetches a single value(or defaults to) from interpreted server argv.
     *
     * A default value can be returned if the key doesn't exist.
     *
     * @param string|int $key     Index / argument
     * @param mixed      $default Default value
     *
     * @return mixed
     */
    public function arg($key, $default = null)
    {
        return static::$args[$key] ?? $default;
    }

    /**
     * Converts $_SERVER['argv'] to something more accessible.
     *
     * <samp>
     * <?php
     * # console -w src/ -x "composer run lint"
     *
     * $argv = \Console\CommandLine::parse($_SERVER['argv']);
     * $argv['w'] === 'src/';
     * $argv['x'] === 'composer run lint';
     *
     * # console --dry-run --watch src/ --exec "composer run lint"
     *
     * $argv = \Console\CommandLine::parse($_SERVER['argv']);
     * $argv['dry-run'] === true;
     * $argv['watch'] === 'src';
     * $argv['exec'] === 'composer run lint';
     * ?>
     * </samp>
     *
     * @param array $argv $_SERVER['argv']
     *
     * @return array
     */
    public static function parse($argv)
    {
        // Skip file name
        array_shift($argv);

        $parsed = [];
        $next = null;

        foreach ($argv as $arg) {
            if ('--' === substr($arg, 0, 2)) {
                $parsed[$next = substr($arg, 2)] = $parsed[$next] ?? true;
            } elseif ('-' === substr($arg, 0, 1)) {
                $parsed[$next = substr($arg, 1)] = $parsed[$next] ?? true;
            } elseif (null !== $next) {
                $set = $parsed[$next];

                if (true === $set) {
                    $parsed[$next] = $arg;
                } elseif (is_array($set)) {
                    $parsed[$next][] = $arg;
                } else {
                    $parsed[$next] = [$set, $arg];
                }

                $next = null;
            }
        }

        return array_filter($parsed, static function ($key) {
            return '' !== str_replace('-', '', $key);
        }, ARRAY_FILTER_USE_KEY);
    }
}
