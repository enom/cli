<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Console;

/**
 * Console help messages helper.
 */
class Help extends Prints
{
    /**
     * Returns the list of examples for the help message.
     *
     * @return array
     */
    public static function getExamples()
    {
        return [];
    }

    /**
     * Returns the note part of the help message.
     *
     * @return array
     */
    public static function getNotes()
    {
        return [];
    }

    /**
     * Returns the list of options for the help message.
     *
     * @return array
     */
    public static function getOptions()
    {
        return [];
    }

    /**
     * Returns the usage part of the help message.
     *
     * @return string
     */
    public static function getUsage()
    {
        return '';
    }

    /**
     * Prints the examples help message.
     */
    public static function printExamples()
    {
        self::p('Examples:');
        self::p();

        foreach (static::getExamples() as $example) {
            self::p('   $ '.$example);
        }
    }

    /**
     * Prints the -h/--help message.
     */
    public static function printHelp()
    {
        self::printUsage();
        self::printOptions();
        self::printNotes();
        self::printExamples();
    }

    /**
     * Prints the note help message.
     */
    public static function printNotes()
    {
        self::p('Notes:');
        self::p();

        foreach (static::getNotes() as $note) {
            self::hardWrap($note, '    ', '   ');
            self::p();
        }
    }

    /**
     * Prints a given option help message.
     *
     * @param string $option      Option to print
     * @param string $description Description to print
     */
    public static function printOption(string $option, string $description)
    {
        // Vertical lineup of option descriptions by padding with dots
        $dots = str_repeat('.', 20 - strlen($option));

        // Option text "   -w, --watch "path" .... "
        $prefix = '   '.$option.' '.$dots;

        // Full option text "   -w, --watch "path" .... Watch directory or..."
        $text = $prefix.' '.$description;

        if (strlen($text) > self::MAX_CHARS) {
            self::hardWrap($text, '  ', str_repeat(' ', 25));
        } else {
            // Print the entire line if it's under 80 characters
            self::p($text);
        }
    }

    /**
     * Prints the options help message.
     */
    public static function printOptions()
    {
        self::p('Options:');
        self::p();

        foreach (static::getOptions() as $option => $description) {
            self::printOption($option, $description);
        }

        self::p();
    }

    /**
     * Prints the usage help message.
     */
    public static function printUsage()
    {
        self::p(static::getUsage());
        self::p();
    }
}
