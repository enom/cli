<?php

/*
 * Copyright 2020 Jonathan Ginn <enom@enom.ws>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Console\CommandLine;

/**
 * Test class for \Console\CommandLine.
 */
class CommandLineTest extends \Codeception\Test\Unit
{
    /**
     * Command line parser.
     *
     * @var \Console\CommandLine
     */
    public $cli;

    /**
     * Returns $_SERVER['argv'].
     *
     * @return array
     */
    public function argv()
    {
        return $_SERVER['argv'];
    }

    /**
     * Reset $_SERVER['argv'] with a given string.
     *
     * @param string $str Command string to reset
     */
    public function reset(string $str = '')
    {
        $script = ['/usr/bin/console'];

        // Parse quoted string arguments
        preg_match_all('/[^\s"\']+|"[^"]*"|\'[^\']*\'/', $str, $matches);

        // Get raw matches
        $command = $matches[0];

        for ($i = 0; $i < count($command); ++$i) {
            // Remove quotes from encapsulated strings
            $command[$i] = trim($command[$i], '\s\'"');
        }

        // Simulating calls from command line
        $_SERVER['argv'] = null === $str ? $command : array_merge($script, $command);

        // Initialize new class to test
        $this->cli = new CommandLine();
    }

    /**
     * Test parsing arguments that show up more than once in a command.
     */
    public function testParseArrayArguments()
    {
        // Useful for specifying multiple directories
        $this->reset('-a one -a two');
        $this->assertCount(5, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertIsArray($this->cli->arg('a'));
        $this->assertCount(2, $this->cli->arg('a'));
        $this->assertEquals(['one', 'two'], $this->cli->arg('a'));

        // More tests
        $this->reset('--alpha one -b two --alpha three');
        $this->assertCount(7, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertIsArray($this->cli->arg('alpha'));
        $this->assertEquals(['one', 'three'], $this->cli->arg('alpha'));
        $this->assertEquals('two', $this->cli->arg('b'));
    }

    /**
     * Test parsing arguments that have dashes in them.
     */
    public function testParseDashArguments()
    {
        // Single and double dash arguments with dashes in them are OK
        $this->reset('-a-b --alpha--bravo one');
        $this->assertCount(4, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertTrue($this->cli->arg('a-b'));
        $this->assertEquals('one', $this->cli->arg('alpha--bravo'));
    }

    /**
     * Test parsing commands with one flag argument.
     */
    public function testParseFlags()
    {
        // Arguments can be set using a single dash
        $this->reset('-a');
        $this->assertCount(2, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertTrue($this->cli->arg('a'));

        // Arguments can be set using a double dash
        $this->reset('--alpha');
        $this->assertCount(2, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertTrue($this->cli->arg('alpha'));

        // Consecutive flags
        $this->reset('-a --bravo');
        $this->assertCount(3, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertTrue($this->cli->arg('a'));
        $this->assertTrue($this->cli->arg('bravo'));

        // Several consecutive flags
        $this->reset('--alpha -b --charlie -d');
        $this->assertCount(5, $this->argv());
        $this->assertCount(4, $this->cli->args());
        $this->assertTrue($this->cli->arg('alpha'));
        $this->assertTrue($this->cli->arg('b'));
        $this->assertTrue($this->cli->arg('charlie'));
        $this->assertTrue($this->cli->arg('d'));
    }

    /**
     * Test parsing commands that are invalid.
     */
    public function testParseInvalid()
    {
        // No arguments passed to command
        $this->reset();
        $this->assertCount(1, $this->argv());
        $this->assertCount(0, $this->cli->args());

        // Arguments must be defined with dashes
        $this->reset('one two three');
        $this->assertCount(4, $this->argv());
        $this->assertCount(0, $this->cli->args());

        // Arguments can't just be dashes
        $this->reset('-');
        $this->assertCount(2, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('--');
        $this->assertCount(2, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('---');
        $this->assertCount(2, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('- -');
        $this->assertCount(3, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('-- -');
        $this->assertCount(3, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('- --');
        $this->assertCount(3, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('-- --');
        $this->assertCount(3, $this->argv());
        $this->assertCount(0, $this->cli->args());

        $this->reset('- -- --- ----');
        $this->assertCount(5, $this->argv());
        $this->assertCount(0, $this->cli->args());
    }

    /**
     * Test parsing commands with mixed flags and values.
     */
    public function testParseMixed()
    {
        $this->reset('-a one --bravo -c --delta two');
        $this->assertCount(7, $this->argv());
        $this->assertCount(4, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('a'));
        $this->assertTrue($this->cli->arg('bravo'));
        $this->assertTrue($this->cli->arg('c'));
        $this->assertEquals('two', $this->cli->arg('delta'));
    }

    /**
     * Test parsing commands with trailing arguments.
     */
    public function testParseTrailingArguments()
    {
        // Command line gobbles trailing arguments
        $this->reset('-a one two');
        $this->assertCount(4, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('a'));

        // Double dash behaves the same
        $this->reset('--alpha one two');
        $this->assertCount(4, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('alpha'));

        // Values before arguments are dropped
        $this->reset('one two -a');
        $this->assertCount(4, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertTrue($this->cli->arg('a'));

        // No values after argument makes it a flag
        $this->reset('one two --alpha');
        $this->assertCount(4, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertTrue($this->cli->arg('alpha'));

        // More values being dropped, left and right of the argument
        $this->reset('one two -a three four --bravo five six');
        $this->assertCount(9, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertEquals('three', $this->cli->arg('a'));
        $this->assertEquals('five', $this->cli->arg('bravo'));
    }

    /**
     * Test parsing commands that set a single value.
     */
    public function testParseValues()
    {
        // Setting an argument like "-d 1"
        $this->reset('-a one');
        $this->assertCount(3, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('a'));

        // All arguments - unless defined as flags - will return strings
        $this->reset('--alpha one');
        $this->assertCount(3, $this->argv());
        $this->assertCount(1, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('alpha'));

        // Anything after an argument is a value, unless it's another argument
        $this->reset('-a one --bravo two');
        $this->assertCount(5, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertEquals('one', $this->cli->arg('a'));
        $this->assertEquals('two', $this->cli->arg('bravo'));

        // Zero returns false?
        $this->reset('-a 0 --bravo 0');
        $this->assertCount(5, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertEquals(false, $this->cli->arg('a'));
        $this->assertEquals(false, $this->cli->arg('bravo'));

        // Every other number is a string
        $this->reset('--alpha 1 -b 2 --charlie 3');
        $this->assertCount(7, $this->argv());
        $this->assertCount(3, $this->cli->args());
        $this->assertEquals('1', $this->cli->arg('alpha'));
        $this->assertEquals('2', $this->cli->arg('b'));
        $this->assertEquals('3', $this->cli->arg('charlie'));

        // Can also handle strings delimited by quotation marks
        $this->reset('-a "a double quote" --bravo \'b single quote\'');
        $this->assertCount(5, $this->argv());
        $this->assertCount(2, $this->cli->args());
        $this->assertEquals('a double quote', $this->cli->arg('a'));
        $this->assertEquals('b single quote', $this->cli->arg('bravo'));
    }
}
