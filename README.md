# CLI

Simple command line argument interpreter.

Supports both shorthand single-dash arguments and extended double-dash arguments.

```php
# console -w src/ -x "composer run lint"

$argv = \Console\CommandLine::parse($_SERVER['argv']);
$argv['w'] === 'src/';
$argv['x'] === 'composer run lint';
```

```php
# console --dry-run --watch src/ --exec "composer run lint"

$argv = \Console\CommandLine::parse($_SERVER['argv']);
$argv['dry-run'] === true;
$argv['watch'] === 'src/';
$argv['exec'] === 'composer run lint';
```
